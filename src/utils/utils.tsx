import { onCleanup } from "solid-js";
import { unwrap } from "solid-js/store";

import { state, setState, setModalState } from "../store/store";

// Types
export enum ButtonType {
  Save,
  Delete,
}

export type Task = {
  id: string;
  groupId: string;
  heading: string;
  description: string;
};

export type Group = {
  heading: string;
  tasks: Array<Task>;
};

// Close Modal from outside
export const clickOutside = (el: any, accessor: any) => {
  const onClick = (e: any) => !el.contains(e.target) && accessor()?.();
  document.body.addEventListener("click", onClick);

  onCleanup(() => document.body.removeEventListener("click", onClick));
};

// Open Modal
export const openModal =
  (groupId: string, taskId: string = "") =>
  () => {
    setModalState({
      show: true,
      groupId,
      taskId,
    });
  };

// Get Modal Task
export const getModalTask = (groupId: number, taskId: string): Task => {
  if (taskId !== "") {
    return state[groupId][taskId];
  }
  return {};
};

// Add Task
let taskId = "A";
const getTaskId = (): string => {
  return taskId;
};

// Add Group
let count = 0;
export const getGroupId = (): number => {
  return ++count;
};

export const addGroup = (heading: string) => {
  console.log("RUNNING addGroup");
  const newGroup: Group = {
    heading,
    tasks: [],
  };
  setState({ [getGroupId()]: newGroup });
  console.log("state is", state);
};
