import type { Component } from "solid-js";
import { For } from "solid-js";

import styles from "./Group.module.css";

import Task from "../Task/Task";
// import Modal from "../Modal/Modal";

import type { Group as GroupType } from "../../utils/utils";
import { openModal } from "../../utils/utils";

const Group: Component<{ id: number; group: GroupType }> = (props) => {
  console.log("props.id is", props.id);
  return (
    <div class={styles.Group}>
      <h1 class={styles.Heading}>{props.group.heading}</h1>
      <ul class={styles.TaskList}>
        <For each={props.group.tasks}>{(task) => <Task task={task} />}</For>
      </ul>
      <p class={styles.AddItem} onClick={openModal(props.id)}>
        + Add Item
      </p>
    </div>
  );
};

export default Group;
