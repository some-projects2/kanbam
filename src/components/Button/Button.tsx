import type { Component } from "solid-js";

import styles from "./Button.module.css";

import { ButtonType } from "../../utils/utils";

const Button: Component<{ type: ButtonType; text: string }> = (props) => {
  return (
    <div
      classList={{
        [styles.Button]: true,
        [styles.Save]: props.type == ButtonType.Save,
        [styles.Delete]: props.type == ButtonType.Delete,
      }}
    >
      {props.text}
    </div>
  );
};

export default Button;
