import type { Component } from "solid-js";

const DescriptionIcon: Component = () => (
  <svg
    id="screenshot"
    viewBox="7122 136 24 24"
    width="24"
    height="24"
    version="1.1"
    xmlns="http://www.w3.org/2000/svg"
    // xmlns:xlink="http://www.w3.org/1999/xlink"
    style="-webkit-print-color-adjust: exact;"
  >
    <g id="shape-04e85a40-efb8-11ec-92f5-dbe82e9c7a50">
      <g id="shape-e4f5a444-efb7-11ec-92f5-dbe82e9c7a50">
        <path
          d="M7122,155.5 h24 a0,0 0 0 1 0,0 v3 a2,2 0 0 1 -2,2 h-20 a2,2 0 0 1 -2,-2 v-3 a0,0 0 0 1 0,0 z"
          x="7122"
          y="155.5"
          transform="matrix(1,0,0,1,0,0)"
          width="24"
          height="5"
          style="fill: rgb(33, 33, 33); fill-opacity: 1;"
        />
      </g>
      <g id="shape-e4f5a443-efb7-11ec-92f5-dbe82e9c7a50">
        <path
          d="M7122,146 h24 a0,0 0 0 1 0,0 v5 a0,0 0 0 1 0,0 h-24 a0,0 0 0 1 0,0 v-5 a0,0 0 0 1 0,0 z"
          x="7122"
          y="146"
          transform="matrix(1,0,0,1,0,0)"
          width="24"
          height="5"
          style="fill: rgb(33, 33, 33); fill-opacity: 1;"
        />
      </g>
      <g id="shape-e4f5a441-efb7-11ec-92f5-dbe82e9c7a50">
        <path
          d="M7124,136.5 h20 a2,2 0 0 1 2,2 v3 a0,0 0 0 1 0,0 h-24 a0,0 0 0 1 0,0 v-3 a2,2 0 0 1 2,-2 z"
          x="7122"
          y="136.5"
          transform="matrix(1,0,0,1,0,0)"
          width="24"
          height="5"
          style="fill: rgb(33, 33, 33); fill-opacity: 1;"
        />
      </g>
    </g>
  </svg>
);

export default DescriptionIcon;
