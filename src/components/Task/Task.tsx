import type { Component } from "solid-js";

import styles from "./Task.module.css";

import { setModalState } from "../../store/store";
import type { Task as TaskType } from "../../utils/utils";
import { openModal } from "../../utils/utils";

const Task: Component<{ task: TaskType }> = (props) => {
  return (
    <div class={styles.Task} onClick={openModal(props.task.id)}>
      {props.task.heading || "TEST"}
    </div>
  );
};

export default Task;
