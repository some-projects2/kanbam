import type { Component } from "solid-js";

const HeadingIcon: Component = () => (
  <svg
    id="screenshot"
    viewBox="7122 76 23 24"
    width="23"
    height="24"
    version="1.1"
    xmlns="http://www.w3.org/2000/svg"
    style="-webkit-print-color-adjust: exact;"
  >
    <g id="shape-7f832ec6-efb7-11ec-92f5-dbe82e9c7a50">
      <rect
        rx="4"
        ry="4"
        x="7122"
        y="76"
        transform="matrix(1,0,0,1,0,0)"
        width="23"
        height="24"
        style="fill: rgb(33, 33, 33); fill-opacity: 1;"
      />
    </g>
  </svg>
);

export default HeadingIcon;
