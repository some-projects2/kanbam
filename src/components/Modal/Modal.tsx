import type { Component } from "solid-js";
import { Portal } from "solid-js/web";

import styles from "./Modal.module.css";

import HeadingIcon from "../HeadingIcon/HeadingIcon";
import DescriptionIcon from "../DescriptionIcon/DescriptionIcon";
import Button from "../Button/Button";

import { ButtonType, clickOutside, getModalTask } from "../../utils/utils";
import { setModalState } from "../../store/store";

const Modal: Component = () => {
  clickOutside; // ...
  return (
    <Portal mount={document.getElementById("modal")}>
      <dialog
        id="modal"
        class={styles.Modal}
        show={false}
        use:clickOutside={() => setModalState({ show: false })}
      >
        <h1 class={styles.Heading}>
          <HeadingIcon />
          Heading
        </h1>
        <p class={styles.Description}>
          <DescriptionIcon />
          Description
        </p>
        <textarea rows={10} class={styles.DescriptionText}>
          Lorem, ipsum dolor sit amet consectetur adipisicing elit. Ab, ipsum!
        </textarea>
        <div class={styles.Buttons}>
          <Button type={ButtonType.Save} text={"Save"} />
          <Button type={ButtonType.Delete} text={"Delete"} />
        </div>
      </dialog>
      //{" "}
    </Portal>
  );
};

export default Modal;
