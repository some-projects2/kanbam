import { createSignal, Show } from "solid-js";
import type { Component } from "solid-js";

import styles from "./AddGroup.module.css";

import { addGroup as AddGroupFunc } from "../../utils/utils";

const AddGroup: Component = () => {
  const [addGroup, setAddGroup] = createSignal(true);
  let heading: any;

  const handleClick = () => {
    setAddGroup(true);
  };

  const handleKeyDown = (e: any) => {
    if (e.key === "Enter") AddGroupFunc(heading.value);
  };

  return (
    <Show
      when={addGroup()}
      fallback={
        <div class={styles.AddGroup} onClick={handleClick}>
          + Add Group
        </div>
      }
    >
      <input
        ref={heading}
        class={styles.AddGroupInput}
        type="text"
        onKeyDown={handleKeyDown}
      />
      ;
    </Show>
  );
};

export default AddGroup;
