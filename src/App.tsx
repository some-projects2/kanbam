import { Show, For } from "solid-js";
import type { Component } from "solid-js";

import AddGroup from "./components/AddGroup/AddGroup";
import Group from "./components/Group/Group";
import Modal from "./components/Modal/Modal";

import styles from "./App.module.css";

import { modalState, state } from "./store/store";

const App: Component = () => {
  console.log("modalState is", modalState.show);
  return (
    <div class={styles.App}>
      <For each={Object.entries(state)}>
        {(group, id) => {
          return <Group group={group[1]} id={id()} />;
        }}
      </For>
      <AddGroup />
      <Show when={modalState.show}>
        <Modal />
      </Show>
    </div>
  );
};

export default App;
