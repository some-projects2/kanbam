import { createStore } from "solid-js/store";
// import type { StoreNode, Store, SetStoreFunction } from "solid-js/store";

import { Task, Group } from "../utils/utils";

export const [modalState, setModalState] = createStore({
  show: false,
  groupId: "",
  taskId: "",
});

const data: { [id: number]: Group } = {
  0: {
    heading: "Todo",
    tasks: [
      {
        id: "a",
        groupId: "0",
        heading: "Research markdown kanban structure",
        description:
          "I decided to use the following:\n# Heading\n- task\n  - description",
      },
      {
        id: "b",
        groupId: "0",
        heading: "Create Prototype",
        description: "-",
      },
      {
        id: "c",
        groupId: "0",
        heading:
          "Multi-line task about doing something in 2 lines or more whatever is needed",
        description: "",
      },
    ],
  },
};
// const data: [Group] = [
//   {
//     id: "0",
//     heading: "Todo",
//     tasks: [
//       {
//         id: "a",
//         groupId: "0",
//         heading: "Research markdown kanban structure",
//         description:
//           "I decided to use the following:\n# Heading\n- task\n  - description",
//       },
//       {
//         id: "b",
//         groupId: "0",
//         heading: "Create Prototype",
//         description: "-",
//       },
//       {
//         id: "c",
//         groupId: "0",
//         heading:
//           "Multi-line task about doing something in 2 lines or more whatever is needed",
//         description: "",
//       },
//     ],
//   },
// ];
export const [state, setState] = createStore(data);
